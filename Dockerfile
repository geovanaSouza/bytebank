FROM python:3.6

# Copy project files to dir /usr/src/app
COPY . /usr/src/app

# Define dir where CMD will be execute and copying the file
WORKDIR /usr/src/app

COPY requirements.txt ./

# Installing requirements with pip
RUN pip install --no-cache-dir -r requirements.txt

# Expose APP Port
EXPOSE 8000

# Execute command to start the app
CMD [ "gunicorn", "to_do.wsgi:application", "--bind", "0.0.0.0:8000", "--workers", "3"]